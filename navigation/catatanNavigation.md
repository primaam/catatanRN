* bagian awal
pertama buka reactnavigation.org

- install yang ada dibawah
npm install @react-navigation/native
expo install react-native-gesture-handler react-native-reanimated react-native-screens react-native-safe-area-context @react-native-community/masked-view

- import ke index.js
import 'react-native-gesture-handler';

- bikin folder navigation , abis itu bikin file AppStack.js abis itu install
npm install @react-navigation/stack

- masukin {createStackNavigator} from @react-navigation/stack
- masukin import {NavigationContainer} from @react-navigation/native
const Stack = createStackNavigator();

masukin semua screen yang ngestack kesini dan import (ciri cirinya kalo back dia ke screen sebelum screeen yang ingin dituju)
masukin screen berurutan dari screen awal yang ditampilin 
- function AppStack(props) {
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                options= {{headerShown: false/true }} =====> ini untuk bagian atasnya ada atau engga
                name= '(nama yang didaftarin buat screen)'
                component={(screen yang digunakan)}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

* bagian profile stack
jadi bagian ini kayak navigasi biasa cuma dibikin route dan bisa jadi kayak fitur bottom tabnya ngikut

- bikin file (contoh) HomeStack.js
- lakuin hal yang sama kayak createStackNavigator

- masukin dan import semua screen yang akan ada di route tersebut 
export default function HomeStack (){
    return(
        <Stack.Navigator>
            <Stack.Screen
            options= {{headerShown: false/true }} =====> ini untuk bagian atasnya ada atau engga
            name= '(nama yang didaftarin buat screen)'
            component={(screen yang digunakan)}
            />
        </Stack.Navigator>
    )
}

* bagian MainNavigation.js
di bagian ini bisa dibilang landing page dan juga screen utama, dan disini awal di set bottom tab
- bikin MainNavigation.js di folder navigation
- install bottom tab navigator 
npm install @react-navigation/bottom-tabs
const Tab = createBottomTabNavigator()

export default function MainNavigation( {
    return (
        <Tab.navigator>
        initialRouteName = 'home' ====> screen pertama kali jika main navigator dipanggil
        screenOptions= {({ route}) => {
            let iconName;

            // membuat ternary untuk style jika dia dipilih atau tidak dipilih beda warna
            if (route.name === 'home') {
                iconName = focused ? 'home' : 'home-outline';  
            } else if (route.name === 'search') {
                iconName = focused ? 'search-circle' : 
            }
        }}
        </Tab.navigator>
    )
})


