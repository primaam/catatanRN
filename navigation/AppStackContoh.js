import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import LoginScreen from '../screen/LoginScreen'
import RegisterScreen from '../screen/RegisterScreen'
import WelcomeScreen from '../screen/WelcomeScreen';
import MainNavigator from './MainNavigator';
import EditProfile from '../screen/EditProfileScreen';
import AddAnimal from '../screen/AddAnimalScreen';
import DetailChatScreen from '../screen/DetailChatScreen';

const Stack = createStackNavigator();

export default function AppStack(props) {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen
                options={{headerShown: false}}
                name='Welcome'
                component={WelcomeScreen}
                />
                <Stack.Screen
                options={{headerShown: false}}
                name='Login'
                component={LoginScreen}
                />
                <Stack.Screen
                options={{headerShown: false}}
                name='Register'
                component={RegisterScreen}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}